SHELL=/bin/bash
RECS=$(shell ls rec/*.rec)
DKS=$(RECS:.rec=.dk)
DKOS=$(RECS:.rec=.dko)
MLS=$(RECS:.rec=.ml)
NATS=$(RECS:.rec=.native)

DKCHECK=dkcheck
DKCHECK_OPTIONS=-nl -coc

TIMEOUT=500

rec2dk.native:
	ocamlbuild src/$@

rec/%.dk: rec/%.rec rec2dk.native
	@echo "Translating $*.rec to Dedukti"
	@cd rec && ../rec2dk.native ../$<

rec/%.dko: rec/%.dk
	@echo "Checking file $*.dk"
	@cd rec && \
	timeout $(TIMEOUT) $(DKCHECK) $(DKCHECK_OPTIONS) -e $*.dk > $*.out || \
	( [[ "$$?" == "124" ]] && echo -e "\033[33mTIMEOUT\033[m" ) || \
	echo -e "\033[31mERROR\033[m"

rec/%.ml: rec/%.rec rec2dk.native
	@echo "Translating $*.rec to OCaml"
	@cd rec && ../rec2dk.native -lang OCaml ../$<

rec/%.native: rec/%.ml
	@echo "Compiling file $*.ml"
	@timeout $(TIMEOUT) ocamlfind ocamlc -package ppx_deriving.show -linkpkg -w -8 -o $@ $<
	@echo "Running $*.native"
	@timeout $(TIMEOUT) $@ > rec/$*.ocaml_out

test: all_dk

fulltest: all_dk all_ocaml

all_dk: $(DKS) $(DKOS)
all_ocaml: $(MLS) $(NATS)

dks: $(DKS)
mls: $(MLS)

clean:
	rm -f rec/*.dk rec/*.dko rec/*.ml rec/*.out rec2dk.native
