(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

 *)

module P = Parsetree

let keywords = ["bool"; "int"; "true"; "false"; "and"; "or"; "xor"; "val";
                "mod"; "try"]

let quote s = if List.mem (String.lowercase s) keywords then s ^ "_" else s

let print_sort_quoted pp = P.print_sort (fun out s -> pp out (quote s))
let print_op_quoted pp = P.print_op (fun out s -> pp out (quote s))

let print_sort = print_sort_quoted Printer.print_uncapitalize
let print_constr = P.print_op Printer.print_capitalize
let print_fun = print_op_quoted Printer.print_uncapitalize
let print_var = print_op_quoted Printer.print_uncapitalize
let print_op (cons, funs) out s =
  if List.mem s funs then print_fun out s
  else if List.mem s cons then print_constr out s
  else print_var out s

let print_term ((cons, funs) as ops) =
  Printer.print_term (fun op -> not (List.mem op cons)) (print_op ops) (print_op ops)

let print_consdecl out opdecl =
  match opdecl.P.od_domain with
  | [] ->
     Format.fprintf out
       "| %a"
       print_constr opdecl.P.od_op
  | dom ->
     Format.fprintf out
       "| @[<2>%a of@ %a@]"
       print_constr opdecl.P.od_op
       (Printer.print_sep_list " * " print_sort) dom

let print_sortdecl out (sort, cons) =
  Format.fprintf out
    "%a =@\n%a@ [@@@@deriving show]"
    print_sort sort
    (Printer.print_newline_list print_consdecl) cons

let print_first_sortdecl out sd =
  Format.fprintf out "@[<2>type %a@]" print_sortdecl sd

let print_other_sortdecl out sd =
  Format.fprintf out "@\n@[<2>and %a@]" print_sortdecl sd

let print_sortdecls out l =
  match l with
  | [] -> ()
  | sd :: l ->
     Format.fprintf out
       "%a@\n%a"
       print_first_sortdecl sd
       (Printer.print_newline_list print_other_sortdecl) l

(* Definitions of recursive fonctions *)
let print_parameter out (var, sort) =
  Format.fprintf out
    "(%a : %a)"
    print_var var
    print_sort sort

let print_cond ops out = function
  | P.Conv (t1, t2) ->
     Format.fprintf out "%a = %a" (print_term ops) t1 (print_term ops) t2
  | P.NConv (t1, t2) ->
     Format.fprintf out "%a <> %a" (print_term ops) t1 (print_term ops) t2

let print_conds ops out = function
  | [] -> ()
  | conds ->
     Format.fprintf out
       " when %a"
       (Printer.print_sep_list " && " (print_cond ops)) conds

let print_rule ops op out rule =
  match rule.P.lhs with
  | P.App (op', args) when op = op' ->
     Format.fprintf out
       "| @[%a%a ->@ %a@]"
       (Printer.print_comma_list (print_term ops)) args
       (print_conds ops) rule.P.conds
       (print_term ops) rule.P.rhs
  | P.Var op' when op = op' ->
     if rule.P.conds <> [] then failwith "Unsupported conditional definition";
     Format.fprintf out
       "@[%a@]@\n"
       (print_term ops) rule.P.rhs
  | _ -> assert false

let print_def ops op sort out body =
  Format.fprintf out
    "%a : %a =@ %a.@]"
    (print_op ops) op
    print_sort sort
    (print_term ops) body

let print_opdef ops out (opdecl, rules) =
  match opdecl.P.od_domain with
  | [] -> begin
      match rules with
      | [rule] ->
         assert (rule.P.lhs = P.Var opdecl.P.od_op);
         if rule.P.conds <> [] then
           failwith "Conditional definitions are not supported";
         Format.fprintf out
           "%a : %a =@ %a"
           (print_op ops) opdecl.P.od_op
           print_sort opdecl.P.od_range
           (print_term ops) rule.P.rhs;
      | [] -> failwith ("Undefined symbol: " ^ P.op_to_string opdecl.P.od_op)
      | _ -> failwith ("Multiple definitions for symbol " ^
                         P.op_to_string opdecl.P.od_op)
    end
  | domain -> begin
      let typed_vars =
        List.mapi (fun i s -> (P.mk_op (Printf.sprintf "x_%d" i), s))
          domain
      in
      let vars = List.map fst typed_vars in
      Format.fprintf out
        "%a %a: %a =@\n"
        (print_op ops) opdecl.P.od_op
        (Printer.print_space_list print_parameter) typed_vars
        print_sort opdecl.P.od_range;
      Format.fprintf out
        "@[<2>match %a with@\n%a@]"
        (Printer.print_comma_list print_var) vars
        (Printer.print_newline_list (print_rule ops opdecl.P.od_op)) rules
    end

let print_opdef_in_fixpoint ops out opdef =
  Format.fprintf out "@[<2>and %a@]@\n"
    (print_opdef ops) opdef

let rec print_recdef ops out = function
  | P.Definition opdef ->
     Format.fprintf out "@[<2>let %a@]@\n"
       (print_opdef ops) opdef
  | P.Fixpoint [] -> failwith ("Empty Fixpoint")
  | P.Fixpoint (opdef :: opdefs) ->
     Format.fprintf out "@[<2>let rec %a@]@\n@\n%a@\n"
       (print_opdef ops) opdef
       (Printer.print_newline_list (print_opdef_in_fixpoint ops)) opdefs

let print_command ops spec out t =
  Format.fprintf out "Format.printf \"%%a@@\\n\" pp_%a (%a);;"
    print_sort (P.typeof t spec)
    (print_term ops) t

let rec sort_defs acc = function
  | [] -> List.rev acc
  | opdef :: l -> begin
     match (fst opdef).P.od_domain with
     | [] -> sort_defs (P.Definition opdef :: acc) l
     | _ -> sort_defs_in_fixpoint acc [opdef] l
    end
and sort_defs_in_fixpoint acc acc2 = function
  | [] -> List.rev (P.Fixpoint (List.rev acc2) :: acc)
  | opdef :: l -> begin
     match (fst opdef).P.od_domain with
     | [] -> sort_defs (P.Definition opdef :: P.Fixpoint (List.rev acc2) :: acc) (l)
     | _ -> sort_defs_in_fixpoint acc (opdef :: acc2) l
    end

let print_spec out spec =
  Format.fprintf out "(* Sorts *)@\n%a@\n@\n"
    print_sortdecls (P.spec_sorts spec);
  let defs = P.spec_ops spec in
  let funs = List.map (fun od -> od.P.od_op) spec.P.ops in
  let cons = List.map (fun od -> od.P.od_op) spec.P.cons in
  let ops = (cons, funs) in
  let defs = sort_defs [] defs in
  Format.fprintf out "(* Functions *)@\n%a@\n@\n"
    (Printer.print_newline_list (print_recdef ops)) defs;
  Format.fprintf out ";;@\n(* Commands *)@\n%a@\n"
    (Printer.print_newline_list (print_command ops spec)) spec.P.command
