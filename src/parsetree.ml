(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

 *)

type 'a printer = Format.formatter -> 'a -> unit

type sort = string
let print_sort pp = pp
let mk_sort s = s
let op_to_string s = s

type op = string
let print_op pp = pp
let mk_op s = s

type var = op

type term =
  | Var of op
  | App of op * term list

type vardecl = { vd_vars : op list; vd_sort : sort }

type opdecl =
  { od_op : op;
    od_domain : sort list;
    od_range : sort }

type cond =
  | Conv of term * term
  | NConv of term * term

type rule = { lhs : term; rhs : term; conds: cond list }

type command = term

type spec =
  { id: string;
    sorts : sort list;
    cons : opdecl list;
    ops : opdecl list;
    vars: vardecl list;
    rules : rule list;
    command : command list}

(* Returns a list of sorts with their constructors *)
let spec_sorts spec =
  List.map (fun sort ->
      (sort, List.filter
               (fun opdecl -> opdecl.od_range = sort)
               spec.cons))
        spec.sorts

type opdef = opdecl * rule list

let term_head t = match t with Var x | App (x, _) -> x

exception Typeof_exception of term

let typeof t spec =
  let hd = term_head t in
  try
    let opdecl =
      List.find (fun od -> od.od_op = hd) (spec.ops @ spec.cons)
    in
    opdecl.od_range
  with
  | Not_found ->
     try
       let vardecl = List.find (fun vd -> List.mem hd vd.vd_vars) spec.vars in
       vardecl.vd_sort
     with Not_found -> raise (Typeof_exception t)

(* Returns a list of operators with their defining rewrite rules *)
let rec specs_ops_aux spec acc = function
  | [] -> List.rev acc
  | rule :: rules ->
     let head = term_head rule.lhs in
     let od = List.find (fun od -> od.od_op = head) (spec.ops @ spec.cons) in
     specs_ops_aux_rules spec acc head od [rule] rules
and specs_ops_aux_rules spec acc head od acc2 = function
  | [] -> List.rev ((od, List.rev acc2) :: acc)
  | rule :: rules ->
     let nhead = term_head rule.lhs in
     if nhead = head then
       specs_ops_aux_rules spec acc head od (rule :: acc2) rules
     else
       let nod = List.find (fun od -> od.od_op = nhead) (spec.ops @ spec.cons) in
       specs_ops_aux_rules spec
         ((od, List.rev acc2) :: acc) nhead nod [rule] rules

let rec spec_ops spec =
  try
    specs_ops_aux spec [] spec.rules
  with Not_found -> failwith "spec_ops"

(* Returns the list of all variables *)
let spec_vars spec =
  List.flatten (List.map (fun vd -> vd.vd_vars) spec.vars)

let insert x l = if List.mem x l then l else x :: l

(* dependencies t acc l is rev acc ++ the dependencies of t to operators in l *)
let rec dependencies t acc l =
  match t with
  | Var x -> if List.mem x l then insert x acc else acc
  | App (x, args) ->
     let acc2 = if List.mem x l then insert x acc else acc in
     list_dependencies args acc2 l
and list_dependencies ts acc l =
  match ts with
  | [] -> List.rev acc
  | t1 :: ts ->
     let ld = list_dependencies ts acc l in
     dependencies t1 ld l

(* Dependencies of an operator to future ones (possibly including itself) *)
let op_dependencies (opdecl, rules) ops =
  list_dependencies (List.map (fun rule -> rule.rhs) rules) []
    (List.map (fun (opdecl, _) -> opdecl.od_op) ops)

let rec ops_recursive_dependencies (ops : op list) all_ops acc =
  match ops with
  | [] -> List.rev acc
  | op :: ops ->
     if List.mem op acc then ops_recursive_dependencies ops all_ops acc else begin
         let opd = List.find (fun (opdecl, _) -> opdecl.od_op = op) all_ops in
         let first_deps = op_dependencies opd all_ops in
         ops_recursive_dependencies (first_deps @ ops) all_ops
           (op :: acc)
       end

type recursive_def =
  | Definition of opdef
  | Fixpoint of opdef list
