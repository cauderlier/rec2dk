(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree

type 'a printer = Format.formatter -> 'a -> unit

let rec print_list (ppsep : unit printer) (ppelem : 'a printer) : 'a list printer =
  fun out l -> match l with
  | [] -> ()
  | [a] -> ppelem out a
  | a :: l -> Format.fprintf out
                "%a%a%a"
                ppelem a
                (fun out -> ppsep out) ()
                (print_list ppsep ppelem) l

let print_sep_list s = print_list (fun out () -> Format.fprintf out "%s" s)

let print_break out () = Format.fprintf out "@ "
let print_newline out () = Format.fprintf out "@\n"
let print_comma out () = Format.fprintf out ",@ "
let print_arrow out () = Format.fprintf out " ->@ "

let print_space_list ppelem out l = print_list print_break ppelem out l
let print_comma_list ppelem out l = print_list print_comma ppelem out l
let print_newline_list ppelem out l = print_list print_newline ppelem out l
let print_arrow_list ppelem out l = print_list print_arrow ppelem out l

let print_string out = Format.fprintf out "%s"
let print_lower out s = Format.fprintf out "%s" (String.lowercase s)
let print_upper out s = Format.fprintf out "%s" (String.uppercase s)
let print_capitalize out s = Format.fprintf out "%s" (String.capitalize s)
let print_uncapitalize out s = Format.fprintf out "%s" (String.uncapitalize s)
let print_int out = Format.fprintf out "%d"

let rec print_term curryfy pvar pop out = function
  | Var v -> pvar out v
  | App (f, args) ->
     if curryfy f then
       Format.fprintf out
         "@[<2>%a %a@]"
         pop f
         (print_space_list (print_term_par curryfy pvar pop)) args
     else
       Format.fprintf out
         "%a (@[%a@])"
         pop f
         (print_comma_list (print_term curryfy pvar pop)) args
and print_term_par curryfy pvar pop out t = match t with
  | Var _ -> print_term curryfy pvar pop out t
  | App _ -> Format.fprintf out "(@[%a@])" (print_term curryfy pvar pop) t
