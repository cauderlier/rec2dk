(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)
module P = Parsetree

(* We redefine list printing functions to avoir pressing limés *)

let print_space_list ppelem out l = Printer.print_sep_list " " ppelem out l
let print_comma_list ppelem out l = Printer.print_sep_list ", " ppelem out l
let print_newline_list = Printer.print_newline_list

let print_sort = P.print_sort Printer.print_string
let print_op = P.print_op Printer.print_string
let print_var = print_op

let print_term = Printer.print_term (fun _ -> false) print_var print_op
let print_sortdecl = print_sort

let print_vardecl out vardecl =
  Format.fprintf out
    "%a : %a"
    (print_space_list print_var) vardecl.P.vd_vars
    print_sort vardecl.P.vd_sort

let print_opdecl out opdecl =
  Format.fprintf out
    "%a : %a -> %a"
    print_op opdecl.P.od_op
    (print_space_list print_sort) opdecl.P.od_domain
    print_sort opdecl.P.od_range

let print_consdecl = print_opdecl

let print_cond out = function
  | Parsetree.Conv (t1, t2) ->
     Format.fprintf out "%a -><- %a" print_term t1 print_term t2
  | Parsetree.NConv (t1, t2) ->
     Format.fprintf out "%a ->/<- %a" print_term t1 print_term t2

let print_rule out rule = match rule.P.conds with
  | [] -> Format.fprintf out
            "%a -> %a"
            print_term rule.P.lhs
            print_term rule.P.rhs
  | _ -> Format.fprintf out
           "%a -> %a if %a"
           print_term rule.P.lhs
           print_term rule.P.rhs
           (print_comma_list print_cond) rule.P.conds

let print_command out t = print_term out t

let print_id = Printer.print_string

let print_section title plist pitem out = function
  | [] -> ()
  | l -> Format.fprintf out
           "@\n@[<2>%s@\n%a@]"
           title
           (plist pitem) l

let print_spec out s =
  Format.fprintf out
    "REC-SPEC %a%a%a%a%a%a%a@\nEND-SPEC@\n"
    print_id s.Parsetree.id
    (print_section "SORTS" print_space_list print_sort) s.Parsetree.sorts
    (print_section "CONS" print_newline_list print_consdecl) s.Parsetree.cons
    (print_section "OPNS" print_newline_list print_opdecl) s.Parsetree.ops
    (print_section "VARS" print_newline_list print_vardecl) s.Parsetree.vars
    (print_section "RULES" print_newline_list print_rule) s.Parsetree.rules
    (print_section "EVAL" print_newline_list print_command) s.Parsetree.command

