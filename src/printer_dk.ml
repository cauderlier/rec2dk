(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)
module P = Parsetree


let print_sort = P.print_sort Printer.print_string
let print_op = P.print_op Printer.print_string
let print_var = print_op

let print_term = Printer.print_term (fun _ -> true) print_var print_op

let print_sortdecl out sort =
  Format.fprintf out "@[<2>%a : Type.@]" print_sort sort

let print_opdecl is_constr out opdecl =
  Format.fprintf out
    "@[<2>%s%a : %a.@]"
    (if not is_constr then "def " else "")
    print_op opdecl.P.od_op
    (Printer.print_arrow_list print_sort)
    (opdecl.P.od_domain @ [opdecl.P.od_range])

let print_cond spec out = function
  | Parsetree.Conv (t1, t2) ->
     Format.fprintf out "@[<2>%a ==@ %a@]" print_term t1 print_term t2
  | Parsetree.NConv (t1, t2) ->
     begin try
         Format.fprintf out "@[<2>rec_eq (%a) (%a) (%a) ==@ rec_false@]"
           print_sort (P.typeof t1 spec)
           print_term t1 print_term t2
       with e ->
         Format.eprintf "Cannot get the type of term %a" print_term t1;
         flush stderr;
         raise e;
     end

let rec occurs var = function
  | Parsetree.Var x -> var = x
  | Parsetree.App (_, ts) -> List.exists (occurs var) ts

let print_rule spec all_vars out rule = match rule.P.conds with
  | [] -> Format.fprintf out
            "@[<2>[%a]@ %a@ -->@ %a.@]"
            (Printer.print_comma_list print_var)
            (List.filter (fun v -> occurs v rule.P.lhs) all_vars)
            print_term rule.P.lhs
            print_term rule.P.rhs
  | _ -> Format.fprintf out
            "@[<2>[%a]@ %a@ ? %a@ -->@ %a.@]"
            (Printer.print_comma_list print_var)
            (List.filter (fun v -> occurs v rule.P.lhs) all_vars)
            print_term rule.P.lhs
            (Printer.print_sep_list " && " (print_cond spec)) rule.P.conds
            print_term rule.P.rhs

let print_command out t =
  Format.fprintf out "@[<2>#EVAL %a.@]" print_term t


(* Dedukti does not support negative conditions so we need to define
   equality. The usual way to do this is to define it
   structurally. However on ground terms the simpler non-confluent
   definition of equality behaves as expected so we use it instead. *)
let preamble out =
  Format.fprintf out "(; Preamble ;)@\n";
  Format.fprintf out "rec_bool : Type.@\n";
  Format.fprintf out "rec_true : rec_bool.@\n";
  Format.fprintf out "rec_false : rec_bool.@\n";
  Format.fprintf out "def rec_eq : A : Type -> A -> A -> rec_bool.@\n";
  Format.fprintf out "[x] rec_eq _ x x --> rec_true@\n";
  Format.fprintf out "[] rec_eq _ _ _ --> rec_false.@\n@\n"

let print_spec out s =
  Format.fprintf out "@[(; #NAME %s. ;)@]@\n@\n" s.Parsetree.id;
  preamble out;
  Format.fprintf out "(; Sorts ;)@\n%a@\n@\n"
    (Printer.print_newline_list print_sortdecl) s.Parsetree.sorts;
  Format.fprintf out "(; Constructors ;)@\n%a@\n@\n"
    (Printer.print_newline_list (print_opdecl true)) s.Parsetree.cons;
  Format.fprintf out "(; Functions ;)@\n%a@\n@\n"
    (Printer.print_newline_list (print_opdecl false)) s.Parsetree.ops;
  Format.fprintf out "(; Rewrite Rules ;)@\n%a@\n@\n"
    (Printer.print_newline_list (print_rule s (Parsetree.spec_vars s)))
    s.Parsetree.rules;
  Format.fprintf out "(; Commands ;)@\n%a@\n"
    (Printer.print_newline_list print_command) s.Parsetree.command
