(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree

type 'a printer = Format.formatter -> 'a -> unit


(* TODO: use breakable spaces *)
(* Helper functions for printing lists *)

(* Most general form: the first parameter is a separator printing function *)
val print_list : unit printer -> 'a printer -> 'a list printer

(* Print one element by line (separated with "@\n") *)
val print_newline_list : 'a printer -> 'a list printer

(* Use the first argument as separator *)
val print_sep_list : string -> 'a printer -> 'a list printer

(* Separate by "@ " (breakable space)  *)
val print_space_list : 'a printer -> 'a list printer

(* Separate by ",@ " *)
val print_comma_list : 'a printer -> 'a list printer

(* Separate by " ->@ " *)
val print_arrow_list : 'a printer -> 'a list printer

val print_string : string printer
val print_lower : string printer
val print_upper : string printer
val print_capitalize : string printer
val print_uncapitalize : string printer

val print_int : int printer

(* The first parameter is a predicate curryfy such that
   curryfy f = true if the term f (t1, ..., tn) should be written
   (f t1 ... tn) *)
val print_term : (op -> bool) -> op printer -> op printer -> term printer

