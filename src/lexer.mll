{

(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)


  open Parser
  exception Unexpected_char of string

  let file = ref ""
  let current_lexbuf = ref None

  let stack = ref []

  let debug_flag = false

  let debug s = if debug_flag then (Printf.eprintf "%s\n" s; flush stderr)

  let push lb = stack := (!file, lb) :: !stack
  let pop () = match !stack with
  | [] -> failwith "empty"
  | (f, top) :: tail ->
      debug ("back to "^f);
      file := f;
      current_lexbuf := Some top;
      stack := tail;
      top

  let quote_char = function
   | '\"' -> "''"
   | c -> String.make 1 c

  let quote_string s =
    let b = Buffer.create (2 * String.length s) in
    String.iter (fun c -> Buffer.add_string b (quote_char c)) s;
    Buffer.contents b

}

let id = ['a'-'z' 'A'-'Z'] ['a'-'z' 'A'-'Z' '_' '0'-'9' ''' '\"']*
let filename = ['a'-'z' 'A'-'Z' '_' '-' '0'-'9' '.']+

rule token = parse
    | [ ' ' '\t' '\r' ] { token lexbuf }
    | '\n' { Lexing.new_line lexbuf; token lexbuf }
    | '%' { comment lexbuf }
    | '(' { LPAR }
    | ')' { RPAR }
    | ',' { COMMA }
    | ';' { COMMA }
    | ':' { COLUMN }
    | "if" { IF }
    | "and-if" { ANDIF }
    | "-><-" { CONV }
    | "->/<-" { NCONV }
    | "->" { ARR }
    | "REC-SPEC" { RECSPEC }
    | "SORTS" { debug "SORTS"; SORTS }
    | "CONS" { debug "CONS"; CONS }
    | "OPNS" { debug "OPNS"; OPNS }
    | "VARS" { debug "VARS"; VARS }
    | "RULES" { RULES }
    | "EVAL" { EVAL }
    | "END-SPEC" { ENDSPEC }
    | "#include \"" (filename as f) "\"" {
         let input = open_in ("../lib/" ^ f) in
         push lexbuf;
         debug ("entering file " ^ f);
         file := f;
         let newlexbuf = Lexing.from_channel input in
         current_lexbuf := Some newlexbuf;
         token newlexbuf
       }
    | id as s { ID(quote_string s) }
    | _ as c { raise (Unexpected_char (Printf.sprintf"'%c'" c)) }
    | eof {try
              let lb = pop () in
              token lb
           with _ -> raise End_of_file }

and comment = parse
    | '\n' { Lexing.new_line lexbuf; token lexbuf }
    | _ { comment lexbuf }
    | eof {try
              let lb = pop () in
              token lb
           with _ -> raise End_of_file }
