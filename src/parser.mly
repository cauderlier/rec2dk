%{

(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

*)

open Parsetree

%}

%token <string> ID
%token LPAR RPAR COMMA COLUMN
%token IF ANDIF CONV NCONV ARR
%token RECSPEC SORTS CONS VARS OPNS RULES EVAL ENDSPEC

%start spec
%type <Parsetree.spec> spec

%%

spec : RECSPEC ID sorts cons ops vars rules commands ENDSPEC
         { {id = $2; sorts = $3; cons = $4; ops = $5; vars = $6; rules = $7; command = $8 } };

cons : CONS opdecllist { $2 };
sorts : SORTS sortlist { $2 };
ops : OPNS opdecllist { $2 };
vars : VARS vardecllist { $2 };
rules : RULES rulelist { $2 };
commands : EVAL command_list { $2 };

command_list : { [] } | command command_list { $1 :: $2 };

sortlist : { [] }
       | ID sortlist { mk_sort $1 :: $2 }
;

oplist : { [] }
       | ID oplist { mk_op $1 :: $2 }
;

vardecl : oplist COLUMN ID { { vd_vars = $1; vd_sort = mk_sort $3 } };

vardecllist : { [] }
            | vardecl vardecllist { $1 :: $2 }
;

opdecllist : { [] }
           | opdecl opdecllist { $1 :: $2 }
;

opdecl : ID COLUMN sortlist ARR ID
         { { od_op = mk_op $1;
             od_domain = $3;
             od_range = mk_sort $5} }
;

rulelist : { [] }
         | rule rulelist { $1 :: $2 }
;

rule : term ARR term { { lhs = $1; rhs = $3; conds = []} }
     | term ARR term IF condlist { { lhs = $1; rhs = $3; conds = $5} }
;

condlist : cond { [$1] } | cond ANDIF condlist { $1 :: $3 }
;

cond : term CONV term { Conv($1, $3) }
     | term NCONV term { NConv($1, $3) }
;

term : ID { Var (mk_op $1) }
     | ID LPAR termlist RPAR { App (mk_op $1, $3) }
;

termlist : term { [$1] }
         | term COMMA termlist { $1 :: $3 }
;

command : term { $1 }
;

%%


