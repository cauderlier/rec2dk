(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is a computer program whose purpose is to translate rewrite systems in the REC format to Dedukti.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

 *)

type 'a printer = Format.formatter -> 'a -> unit

type op
val mk_op : string -> op
val print_op : string printer -> op printer
val op_to_string : op -> string

type sort
val mk_sort : string -> sort
val print_sort : string printer -> sort printer

type term =
  | Var of op
  | App of op * term list

type vardecl = { vd_vars : op list; vd_sort : sort }

type opdecl =
  { od_op : op;
    od_domain : sort list;
    od_range : sort }

type cond =
  | Conv of term * term
  | NConv of term * term

type rule = { lhs : term; rhs : term; conds: cond list }

type command = term

type spec =
  { id: string;
    sorts : sort list;
    cons : opdecl list;
    ops : opdecl list;
    vars: vardecl list;
    rules : rule list;
    command : command list}

(* Returns a list of sorts with their constructors *)
val spec_sorts : spec -> (sort * opdecl list) list

type opdef = opdecl * rule list

(* Returns a list of operators with their defining rewrite rules *)
val spec_ops : spec -> opdef list

(* Returns the list of all variables *)
val spec_vars : spec -> op list

(* Dependencies of an operator to future ones (possibly including itself) *)
val op_dependencies : opdef -> opdef list -> op list

(* Recursive dependencies of a set of operators with an accumulator *)
val ops_recursive_dependencies : op list -> opdef list -> op list -> op list

type recursive_def =
  | Definition of opdef
  | Fixpoint of opdef list

exception Typeof_exception of term
val typeof : term -> spec -> sort
