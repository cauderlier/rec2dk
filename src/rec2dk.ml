(*

Copyright Raphaël Cauderlier (2018)

<raphael.cauderlier@irif.fr>

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided
only with a limited warranty and the software's author, the holder of
the economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards
their requirements in conditions enabling the security of their
systems and/or data to be ensured and, more generally, to use and
operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.

 *)

let wrap lb =
  match !Lexer.current_lexbuf with
  | Some lb -> lb
  | None -> lb

let print_parsing_error lb =
  let lb = wrap lb in
  let start = lb.Lexing.lex_start_p in
  let file = !Lexer.file in
  let line = start.Lexing.pos_lnum  in
  let cnum = start.Lexing.pos_cnum - start.Lexing.pos_bol in
  let tok = Lexing.lexeme lb in
  Format.eprintf "File: %s, line: %d, column: %d, Unexpected token \"%s\"@\n@."
    file line cnum tok

let parse lb =
  try
    Parser.spec (fun lb -> Lexer.token (wrap lb)) lb
  with e -> print_parsing_error lb; raise e

type lang = Dedukti | Rec | OCaml
let extension = function Dedukti -> "dk" | Rec -> "rec" | OCaml -> "ml"
let name = function Dedukti -> "Dedukti" | Rec -> "Rec" | OCaml -> "OCaml"
let all_languages = [Dedukti; Rec; OCaml]

let lang_printer = function
  | Dedukti -> Printer_dk.print_spec
  | Rec -> Printer_rec.print_spec
  | OCaml -> Printer_ocaml.print_spec

let output_lang = ref Dedukti

let set_lang s =
  output_lang :=
    match s with
    | "dk" | "dedukti" | "Dedukti" -> Dedukti
    | "rec" | "Rec" | "REC" -> Rec
    | "ocaml" | "OCaml" | "Ocaml" -> OCaml
    | s -> failwith ("Unknown language: " ^ s)

let main () =
  Arg.parse [("-lang",
              Arg.String set_lang,
              Printf.sprintf
                "Output language (default: %s, available: %s)"
                (name !output_lang)
                (String.concat ", " (List.map name all_languages)))]
    (fun file ->
      let input = open_in file in
      let basefile = Filename.chop_extension (Filename.basename file) in
      let out =
        Format.formatter_of_out_channel
          (open_out (basefile ^ "." ^ extension !output_lang))
      in
      let lexbuf = Lexing.from_channel input in
      Lexer.file := file;
      let spec = parse lexbuf in
      lang_printer !output_lang out spec
    )
    "Usage: rec2dk [options] file.rec

Translate a file in REC format.

Options:"

let _ = main ()
